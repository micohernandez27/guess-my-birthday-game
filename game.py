import random

user_name = input("Hi, What is your name?")


for i in range(5):
    guess_month = random.randint(1,12)
    guess_year = random.randint(1924, 2004)

    print(user_name, "were you born in ", guess_month, "/", guess_year, "?")
    answer = input("yes or no?")

    if answer == "yes":
        print("I knew it!")
        break
    elif i == 4:
        print("I have better things to do")
        break
    else:
        print("Dang it!")
